"""
=========================
BAG2 Via parameter module 
=========================

Collection of Via related technology para,eters for 
BAG2 framework

Initially created by Marko Kosunen, marko.kosunen@aalto.fi, 11.04.2022.

The goal of the class is to provide a master source for the Via parameters 
and enable documentation of those parameters with docstrings.

Documentation instructions
--------------------------

Current docstring documentation style is Numpy
https://numpydoc.readthedocs.io/en/latest/format.html

"""


import os
import pkg_resources
import yaml

from BAG2_technology_template.via_parameters_template import via_parameters_template

class via_parameters(via_parameters_template):

    @property
    def via_units(self):
        '''Property that returns a dict of via units of instances of class 'via_unit'

        '''
        if not hasattr(self,'_via_units'):

            self._via_units= {}

            name = '1'
            # Next unit
            self._via_units[name]=via_unit(name = name)
            self._via_units[name].square = {
                #Dimensions
                'dim' : [30, 30],
                # via horizontal/vertical spacing
                'sp': [36, 36],
                # list of valid via horizontal/vertical spacings when it's a 2x2 array
                # 'sp2': [[int , int ]],
                # list of valid via horizontal/vertical spacings when it's a mxn array,
                # where min(m, n) >= 2, max(m, n) >= 3
                # 'sp3' : [[int , int ]],
                # via bottom enclosure rules.  If empty, then assume it's the same
                # as top enclosure.
                'bot_enc' : None,
                # via top enclosure rules.
                'top_enc' : {
                    # via enclosure rule as function of wire width.
                    'w_list': [float('Inf')],
                    # via enclosure rules corresponding to each width in w_list.  enc_list[idx]
                    # is used if wire width is less than or equal to w_list[idx].
                    'enc_list' : [[[11, 11]]]
                }
            }

            name = '2'
            # Next unit
            self._via_units[name]=via_unit(name = name)
            self._via_units[name].square = {
                #Dimensions
                'dim' : [30, 30],
                # via horizontal/vertical spacing
                'sp': [36, 36],
                # list of valid via horizontal/vertical spacings when it's a 2x2 array
                # 'sp2': [[int , int ]],
                # list of valid via horizontal/vertical spacings when it's a mxn array,
                # where min(m, n) >= 2, max(m, n) >= 3
                # 'sp3' : [[int , int ]],
                # via bottom enclosure rules.  If empty, then assume it's the same
                # as top enclosure.
                'bot_enc' : None,
                # via top enclosure rules.
                'top_enc' : {
                    # via enclosure rule as function of wire width.
                    'w_list': [float('Inf')],
                    # via enclosure rules corresponding to each width in w_list.  enc_list[idx]
                    # is used if wire width is less than or equal to w_list[idx].
                    'enc_list' : [[[0, 0]]]
                }
            }

            name = '3'
            # Next unit
            self._via_units[name]=via_unit(name = name)
            self._via_units[name].square = {
                #Dimensions
                'dim' : [30, 30],
                # via horizontal/vertical spacing
                'sp': [36, 36],
                # list of valid via horizontal/vertical spacings when it's a 2x2 array
                # 'sp2': [[int , int ]],
                # list of valid via horizontal/vertical spacings when it's a mxn array,
                # where min(m, n) >= 2, max(m, n) >= 3
                # 'sp3' : [[int , int ]],
                # via bottom enclosure rules.  If empty, then assume it's the same
                # as top enclosure.
                'bot_enc' : None,
                # via top enclosure rules.
                'top_enc' : {
                    # via enclosure rule as function of wire width.
                    'w_list': [float('Inf')],
                    # via enclosure rules corresponding to each width in w_list.  enc_list[idx]
                    # is used if wire width is less than or equal to w_list[idx].
                    'enc_list' : [[[0, 0]]]
                }
            }

            name = '4'
            # Next unit
            self._via_units[name]=via_unit(name = name)
            self._via_units[name].square = {
                #Dimensions
                'dim' : [30, 30],
                # via horizontal/vertical spacing
                'sp': [36, 36],
                # list of valid via horizontal/vertical spacings when it's a 2x2 array
                # 'sp2': [[int , int ]],
                # list of valid via horizontal/vertical spacings when it's a mxn array,
                # where min(m, n) >= 2, max(m, n) >= 3
                # 'sp3' : [[int , int ]],
                # via bottom enclosure rules.  If empty, then assume it's the same
                # as top enclosure.
                'bot_enc' : None,
                # via top enclosure rules.
                'top_enc' : {
                    # via enclosure rule as function of wire width.
                    'w_list': [float('Inf')],
                    # via enclosure rules corresponding to each width in w_list.  enc_list[idx]
                    # is used if wire width is less than or equal to w_list[idx].
                    'enc_list' : [[[0, 0]]]
                }
            }

        return self._via_units

    @property
    def via_name(self):
        ''' Dictionary of via types between layer types.

        '''
        return {
            1 : '1', # mcon between li1 and met1
            2 : '2', # via between met1 and met2
            3 : '3', # via2 between met2 and met3
            4 : '4', # via3 between met3 and met4
            5 : '5', # via4 between met4 and met5
        }
        
    @property
    def via_id(self):
        ''' Dictionary for mapping a via to connect of two layes to a 
        via primitive of the vendor.

        '''
        return {
            (('poly', 'drawing'), 'li1'): 'TPL1_C',
            (('diff', 'drawing'), 'li1'): 'PYL1_C',
            ('li1', 'met1'): 'L1M1_C', 
            ('met1', 'met2'): 'M1M2_C', 
            ('met2', 'met3'): 'M2M3_C', 
            ('met3', 'met4'): 'M3M4_C', 
            ('met4', 'met5'): 'M4M5_C', 
        }

    @property
    def property_dict(self):
        '''Collection dictionary of all properties of this class.
        This dictionary provides compatibility with the original BAG2 parameter calls.

        '''
        if not hasattr(self, '_property_dict'):
           # yaml_file = pkg_resources.resource_filename(__name__, os.path.join('tech_params.yaml'))
           # with open(yaml_file, 'r') as content:
           #    self._process_config = {}
           #    dictionary = yaml.load(content, Loader=yaml.FullLoader )

           # self._property_dict = dictionary['via']
            self._property_dict = {}
            for key, val  in vars(type(self)).items():
                if isinstance(val,property) and key != 'property_dict':
                    self._property_dict[key] = getattr(self,key)

            for key, val  in self.via_units.items():
                    self._property_dict[key] = val.property_dict
        return self._property_dict

class via_unit:
    def __init__(self,**kwargs):
        if kwargs.get('name'):
            self._name=kwargs.get('name')
        else:
            print('Must give name for a via instance')
            
    @property
    def name(self):
        if not hasattr(self,'_name'):
            self._name = None
        return self._name

    @property
    def square(self):
        if not hasattr(self,'_square'):
            self._square = None
        return self._square

    @square.setter
    def square(self,val):
        self._square = val

    @property
    def hrect(self):
        if not hasattr(self,'_hrect'):
            self._hrect = self._square
        return self._hrect

    @hrect.setter
    def hrect(self,val):
        self._hrect = val

    @property
    def property_dict(self):
        '''Collection dictionary of all properties of this class.
        This dictionary provides compatibility with the original BAG2 parameter calls.

        '''

        if not hasattr(self, '_property_dict'):
            self._property_dict = {}

            for key, val  in vars(type(self)).items():
                if isinstance(val,property) and key != 'property_dict':
                    self._property_dict[key] = getattr(self,key)
        return self._property_dict
