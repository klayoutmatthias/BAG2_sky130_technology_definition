v {xschem version=3.0.0 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N -70 -0 -40 -0 {
lab=G}
N 0 -60 0 -30 {
lab=S}
N 0 30 -0 60 {
lab=D}
N 0 -0 60 0 {
lab=B}
N -70 60 0 60 {
lab=D}
N -70 -60 0 -60 {
lab=S}
C {devices/iopin.sym} -70 60 0 1 {name=p1 lab=D}
C {devices/iopin.sym} -70 0 0 1 {name=p2 lab=G}
C {devices/iopin.sym} -70 -60 0 1 {name=p3 lab=S}
C {devices/iopin.sym} 60 0 0 0 {name=p4 lab=B}
C {cds_ff_mpt/p1svt.sym} -20 0 0 0 {name=P0
l=l
tfin=14n
nfin=10
nf=nf
fpitch=48n
model=cds_ff_mpt_psvt
spiceprefix=X
}
